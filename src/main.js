const data = [
    {
        name: 'Regina',
        base: 'tomate',
        price_small: 6.5,
        price_large: 9.95,
        image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
    },
    {
        name: 'Napolitaine',
        base: 'tomate',
        price_small: 6.5,
        price_large: 8.95,
        image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
    },
    {
        name: 'Spicy',
        base: 'crème',
        price_small: 5.5,
        price_large: 8,
        image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
    }
];



const html = data.filter(({name}) => name.split("").filter(n => n === "i").length === 2).reduce((previous, {
    image,
    name,
    price_large,
    price_small
}) => {
    return previous + `<article class="pizzaThumbnail">
                <a href="${image}">
                    <img src="${image}" alt="${name}"/>
                    <section>
                        <h4>${name}</h4>
                        <ul>
                            <li>Prix petit format : ${price_small} €</li>
                            <li>Prix grand format : ${price_large} €</li>
                        </ul>
                    </section>
                </a>
            </article>`;
}, "");

document.querySelector('.pageContent').innerHTML = html;